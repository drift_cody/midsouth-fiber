<footer class="footer">

	<div class="footer__content gpad">
		<div class="footer__logo-box">
			<div class="left">
				<p class="footer__superscript">A MidSouth Electric Cooperative</p>
				<a href="/" class="footer__logo "><img src="<?php echo esc_url(the_field('header_logo', 'option')) ?>" alt=""></a>
			</div>
			<a href="https://mybroadbandaccount.com/MidSouthFiber/" target="_blank" class="outline-button -orange">Log into account</a>
		</div>
		<div class="footer__navigation">
			<div class="footer__menu">
				<p class="footer__menu-title">Services</p>
				<ul class="footer__link-list">
					<li class="footer__list-item"><a class="footer__link" href="/residential-services">Residential</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/business-services">Business</a></li>
				</ul>
			</div>
			<div class="footer__menu menu">
				<p class="footer__menu-title">Resources</p>
				<ul class="footer__link-list">
					<li class="footer__list-item"><a class="footer__link" href="/blog">News</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/faq">FAQ</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/privacy-policy/">Privacy Policy</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/terms-and-conditions/">Terms and Conditions</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/acceptable-use-policy/">Acceptable Use Policy</a></li>
				</ul>
			</div>
			<div class="footer__menu ">
				<p class="footer__menu-title">Quick Links</p>
				<ul class="footer__link-list">

					<li class="footer__list-item"><a class="footer__link" href="https://mss.crowdfiber.com/" target="_blank">Fiber Areas</a></li>
					<li class="footer__list-item"><a class="footer__link" href="https://mss.crowdfiber.com/front_end/comments" target="_blank">Speed Test</a></li>
					<li class="footer__list-item"><a class="footer__link" href="/contact">Contact</a></li>

				</ul>
			</div>
		</div>
		<div class="footer__social">
			<ul class="c-social-links">
				<li class="c-social-links__item"><a class="c-social-links__link icon-facebook" href="<?php the_field('facebook', 'option'); ?> " target="_blank"></a></li>
				<li class="c-social-links__item"><a class="c-social-links__link icon-instagram" href="<?php the_field('instagram', 'option'); ?> " target="_blank"></a></li>
				<li class="c-social-links__item"><a class="c-social-links__link icon-twitter" href="<?php the_field('twitter', 'option'); ?> " target="_blank"></a></li>
				<li class="c-social-links__item"><a class="c-social-links__link icon-linked-in" href="<?php the_field('linked_in', 'option'); ?> " target="_blank"></a></li>
			</ul>
		</div>
	</div>
	<div class="footer__copyright gpad">
		<p>A Touchstone Energy Cooperative</p>
		<p>MidSouth Fiber Copyright ©2019</p>
	</div>
</footer>
<script>
	$('.header__hamburger').click(function() {
		$(this).toggleClass('active');
		$(".header-links").toggleClass('active');
		$("body").toggleClass('no-scroll');
		$("html").toggleClass('no-scroll');
		if ($(".header-links").hasClass('active')) {
			$(this).attr("aria-expanded", "true");
			$(this).attr("aria-label", "Click to Close Menu");
		} else {
			$(this).attr("aria-expanded", "false");
			$(this).attr("aria-label", "Mobile Menu click to open");
		}
	});
</script>
<?php wp_footer(); ?>



<script type = "text/javascript" src = 'https://home-c13.incontact.com/inContact/ChatClient/js/embed.min.js'></script>
<script type ="text/javascript">
icPatronChat.init({serverHost:'https://home-c13.incontact.com',bus_no:4594668,poc:'3743d0bc-a270-4aba-adbb-8e23a9929614',params:['FirstName','Last Name','first.last@company.com',555-555-5555]});
</script>
</body>

</html>