	<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];
?>

<main class="post">
	<section class="back">
		<a href="/blog" class="icon-caret">Back to news</a>
	</section>
	<section class="post">
			<div class="heading-with-circle">
				<?php the_title('<h1 class="b75 heading-with-circle__heading">', '</h1>'); ?>
				<div class="heading-with-circle__circle-container">
					<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

						<!-- top circle -->
						<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
					</svg>
				</div>
			</div>
			<h2 class="the-date -uline r28"><?php echo the_time('F j, Y'); ?></h2>
		<div class="content">
			<?php the_content(); ?>
		</div>
		<div class="post-nav">
			<?php
				$next_post = get_next_post();
				$prev_post = get_previous_post();
			?>
				<?php if(!empty($prev_post)):?>
					<a class="prev-post icon-caret" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">View Previous Post</a></p>
				<?php endif;?>
				<?php if (!empty( $next_post )): ?>
					<a class="next-post icon-caret" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">View Next Post</a></p>
				<?php endif;?>
				
		</div>
	</section>

</main>
<script>
    function isInView(elem){
		var docTop=$(window).scrollTop();
		var docBottom = docTop + $(window).height();
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();
		return((elemBottom <= docBottom) && elemTop >= docTop);
	}
	$(window).scroll(function(){
		$('.heading-with-circle').each(function(){
			if(isInView(this) === true){
				$(this).addClass('animate');
			}
		});
	});
	$(document).ready(function() {
        $('.heading-with-circle__circle').each(function(){
			if(isInView(this) === true){
				$(this).addClass('animate');
			}
		});
	});
</script>
<?php endwhile; else: ?>

<?php endif; ?>

<?php get_footer(); ?>