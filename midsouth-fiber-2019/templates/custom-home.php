<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

<main class="home">
	<section class="s1-home gpad">
		<div class="left">
			<?php the_field('s1_text'); ?>
		</div>
		<div class="right">
			<?php if (!empty(get_field('s1_image'))) {
				$image = get_field('s1_image'); ?>
				<img <?php acf_srcset($image['id'], 'large', '100vw'); ?> alt="<?php echo $image['alt']; ?>" />
			<?php } ?>
		</div>
	</section>
	<section class="s2-home gpad">
		<div class="left">
			<p class="r28">Ditch the roof ornament. <span>Join MidSouth Fiber</span></p>
		</div>
		<div class="right">
			<script src="https://mss.crowdfiber.com/embed/36.js"></script>
		</div>
	</section>
	<section class="s3-home gpad">
		<h2 class="b35 -uline s3-header"><?php the_field('s3_header'); ?></h2>
		<div class="services">
			<?php
			if (have_rows('s3_services')) :
				while (have_rows('s3_services')) : the_row(); ?>
					<div class="service">
						<div class="img-cover">
							<img src="<?php echo esc_url(get_sub_field('image')[url]); ?>" alt="" class="cover">
						</div>
						<?php the_sub_field('text'); ?>
						<div class="bottom">
							<a href="<?php echo esc_url(get_sub_field('button')['url']); ?>" class="outline-button -orange"><?php echo esc_html(get_sub_field('button')['title']); ?></a>
						</div>
					</div>
			<?php endwhile;
			endif;
			?>
		</div>
		<div class="packages-and-bundles">
			<div class="left">
				<div class="heading-with-circle">
					<?php the_field('s3_left_text') ?>
					<div class="heading-with-circle__circle-container">
						<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

							<!-- top circle -->
							<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
					</div>
				</div>
			</div>
			<div class="right">
				<?php the_field('s3_right_text') ?>
			</div>
		</div>
		<div class="checklist">
			<div class="left">
				<div class="img-cover">
					<img src="<?php echo esc_url(get_field('s3_left_image')['url']); ?>" alt="" class="cover">
				</div>
				<div class="button-container">
					<a href="<?php echo esc_url(get_field('s3_button_link')['url']); ?>" target="_blank" class="solid-button -blue"><?php echo esc_html(get_field('s3_button_link')['title']); ?></a>
				</div>
			</div>
			<div class="right">
				<?php the_field('s3_right_list'); ?>
			</div>
			<div class="button-container mobile">
				<a href="<?php echo esc_url(get_field('s3_button_link')['url']); ?>" class="solid-button -blue"><?php echo esc_html(get_field('s3_button_link')['title']); ?></a>
			</div>
		</div>
	</section>
	<section class="s4-home">
		<div class="-content gpad">
			<div class="left">
				<div class="heading-with-circle">
					<?php the_field('s4_left_text') ?>
					<div class="heading-with-circle__circle-container">
						<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

							<!-- top circle -->
							<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
					</div>
				</div>
			</div>
			<div class="right">
				<?php the_field('s4_middle_text'); ?>
				<a href="<?php echo esc_url(get_field('s4_button')['url']); ?>" class="outline-button -orange"><?php echo esc_html(get_field('s4_button')['title']); ?></a>
			</div>
			<div class="mobile button-container">
				<a href="<?php echo esc_url(get_field('s4_button')['url']); ?>" class="outline-button -orange"><?php echo esc_html(get_field('s4_button')['title']); ?></a>
			</div>
		</div>
		<div class="photo-row-1x2">
			<div class="photo-row-1x2__photo">
				<div class="photo-row-1x2__image-container">
					<img src="<?php echo esc_url(get_field('s4_left_image')['url']); ?>" alt="" class="cover">
				</div>
			</div>
			<div class="photo-row-1x2__photo">
				<div class="photo-row-1x2__image-container">
					<img src="<?php echo esc_url(get_field('s4_right_image')['url']); ?>" alt="" class="cover">
				</div>
			</div>
		</div>
		<div class="compare gpad">
			<h2 class="b35 -uline"><?php the_field('compare_header_text'); ?></h2>
			<div class="options">
				<div class="left">
					<p class="options-list-header">MidSouth Fiber</p>
					<?php
					if (have_rows('midsouth_fiber_options')) :  $i = 1;  ?>
						<ul class="speeds">
							<?php while (have_rows('midsouth_fiber_options')) : the_row(); ?>
								<li class="<?php if ($i === 1) : echo 'active';
													endif; ?>" data-streaming-text="<?php echo get_sub_field('streaming')['text']; ?>" data-streaming-time="<?php echo get_sub_field('streaming')['time']; ?>" data-music-text="<?php echo get_sub_field('music')['text']; ?>" data-music-time="<?php echo get_sub_field('music')['time']; ?>" data-photo-text="<?php echo get_sub_field('photo')['text']; ?>" data-photo-time="<?php echo get_sub_field('photo')['time']; ?>" data-gaming-text="<?php echo get_sub_field('gaming')['text']; ?>" data-gaming-time="<?php echo get_sub_field('gaming')['time']; ?>">
									<?php the_sub_field('speed'); ?><span>Mbps</span></li>
							<?php $i++;
								endwhile; ?>
						</ul>
					<?php endif;	?>
				</div>
				<div class="right">
					<p class="options-list-header">Local Companies</p>
					<?php
					if (have_rows('competitor_options')) : ?>
						<ul class="speeds">
							<?php while (have_rows('competitor_options')) : the_row(); ?>
								<li data-streaming-text="<?php echo get_sub_field('streaming')['text']; ?>" data-streaming-time="<?php echo get_sub_field('streaming')['time']; ?>" data-music-text="<?php echo get_sub_field('music')['text']; ?>" data-music-time="<?php echo get_sub_field('music')['time']; ?>" data-photo-text="<?php echo get_sub_field('photo')['text']; ?>" data-photo-time="<?php echo get_sub_field('photo')['time']; ?>" data-gaming-text="<?php echo get_sub_field('gaming')['text']; ?>" data-gaming-time="<?php echo get_sub_field('gaming')['time']; ?>">
									<?php the_sub_field('speed'); ?><span>Mbps</span></li>
							<?php endwhile; ?>
						</ul>
					<?php endif;	?>
				</div>
			</div>
			<div class="charts">
				<div class="chart streaming-chart">
					<div class="chart__graph">
						<svg class=" chart__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">
							<!-- bottom circle -->
							<circle stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<!-- top circle -->
							<circle class="circle-chart__circle chart__circle-tag streaming" stroke="#0069A7" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
						<div class="chart__icon icon-media"></div>
					</div>
					<div class="chart__text">
						<p class="r28">Streaming</p>
						<p class="r20 change-variable-streaming"></p>
						<p class="r20">1080 HD</p>
					</div>
				</div>
				<div class="chart music-chart">
					<div class="chart__graph">
						<svg class=" chart__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">
							<!-- bottom circle -->
							<circle stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<!-- top circle -->
							<circle class="circle-chart__circle chart__circle-tag music" stroke="#0069A7" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
						<div class="chart__icon icon-music"></div>
					</div>
					<div class="chart__text">
						<p class="r28">Music</p>
						<p class="r20 change-variable-music"></p>
						<p class="r20">10 Songs</p>
					</div>
				</div>
				<div class="chart photo-chart">
					<div class="chart__graph">
						<svg class=" chart__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">
							<!-- bottom circle -->
							<circle stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<!-- top circle -->
							<circle class="circle-chart__circle chart__circle-tag photo" stroke="#0069A7" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
						<div class="chart__icon icon-camera"></div>
					</div>
					<div class="chart__text">
						<p class="r28">Photos</p>
						<p class="r20 change-variable-photo"></p>
						<p class="r20">20 Photos</p>
					</div>
				</div>
				<div class="chart gaming-chart">
					<div class="chart__graph">
						<svg class=" chart__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://wwwf-.w3.org/2000/svg">
							<!-- bottom circle -->
							<circle stroke="#efefef" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
							<!-- top circle -->
							<circle class="circle-chart__circle chart__circle-tag gaming" stroke="#0069A7" stroke-width="1" stroke-dasharray="100,100" stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
						<div class="chart__icon icon-controller"></div>
					</div>
					<div class="chart__text">
						<p class="r28">Gaming</p>
						<p class="r20 change-variable-gaming"></p>
						<p class="r20">500 MB</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="s5-home gpad">
		<div class="c-news-update ">
			<h2 class="c-news-update__header b35 -uline">News & updates</h2>

			<ul class="c-news-update__list">
				<?php echo facetwp_display('template', 'blog_posts'); ?>
			</ul>
		</div>
		<div class="right">
			<div class="facts-slider">
				<div class="facts-slider__logo">
					<img src="<?php the_field('circular_logo', 'option'); ?>" alt="" class="">
				</div>
				<h2 class="facts-slider__header b35 -uline">Did you know?</h2>
				<div class="facts-slider__nav-buttons ">
					<div class="facts-slider__nav-button fact-prev icon-caret"></div>
					<div class="facts-slider__nav-button fact-next icon-caret"></div>
				</div>
				<div class="fact-slideshow">
					<?php if (have_rows('s5_fiber_facts')) : ?>
						<?php while (have_rows('s5_fiber_facts')) : the_row(); ?>
							<div class="facts-slider__slide slide fact">
								<?php the_sub_field('text'); ?>
							</div>
					<?php endwhile;
					endif; ?>
				</div>
			</div>
			<div class="social">
				<h2 class="b35">Let's Get Social</h2>
				<ul class="c-social-links">
					<li class="c-social-links__item"><a href="<?php the_field('facebook', 'option'); ?> " target="_blank" class="icon-facebook c-social-links__link"></a></li>
					<li class="c-social-links__item"><a href="<?php the_field('instagram', 'option'); ?> " target="_blank" class="icon-instagram c-social-links__link"></a></li>
					<li class="c-social-links__item"><a href="<?php the_field('twitter', 'option'); ?> " target="_blank" class="icon-twitter c-social-links__link"></a></li>
					<li class="c-social-links__item"><a href="<?php the_field('linked_in', 'option'); ?> " target="_blank" class="icon-linked-in c-social-links__link"></a></li>
				</ul>
			</div>
		</div>
	</section>
</main>

<script>
	
	// function isInView(elem) {
	// 	var docTop = $(window).scrollTop();
	// 	var docBottom = docTop + $(window).height();
	// 	var elemTop = $(elem).offset().top;
	// 	var elemBottom = elemTop + $(elem).height();
	// 	return ((elemBottom <= docBottom) && elemTop >= docTop);
	// }
	
	// $(window).on('scroll', function() {
	// 	$('.heading-with-circle__circle').each(function() {
	// 		if (isInView(this) === true) {
	// 			console.log("is in view");
	// 			$(this).addClass('animate');
	// 		}
	// 	});
	// });

	$(document).ready(function() {
		$(".s1-home h1").addClass("b130");
		$(".s1-home p").addClass("r35");
		$(".s3-home .service h3").addClass("b28");
		$(".s3-home .service p").addClass("r18");
		$(".packages-and-bundles h2").addClass("r75");
		$(".packages-and-bundles h2").addClass("heading-with-circle__heading");
		$(".packages-and-bundles p").addClass("r28");
		$(".s4-home .-content .left h2").addClass("r75");
		$(".s4-home .-content .left p").addClass("r28");
		$(".s4-home .-content .right p").addClass("r20");
		$(".s5-home .slide.fact p").addClass("r20");
		$(".s5-home .slide.fact h3").addClass("r28");

		//swapChart(false);
		$('ul.speeds li').click(function() {
			$('li.active').removeClass('active');
			$that = $(this);
			$(".circle-chart__circle").css("animation-name", "unset");
			$(".circle-chart__circle").addClass("reverse");
			$($that).addClass('active');
			//LIST ITEM ATTR VARIABLES
			$activeListItem = $(".options li.active");
			$streamingText = $activeListItem.data("streaming-text");
			$streamingTime = $activeListItem.data("streaming-time");
			$musicText = $activeListItem.data("music-text");
			$musicTime = $activeListItem.data("music-time");
			$photoText = $activeListItem.data("photo-text");
			$photoTime = $activeListItem.data("photo-time");
			$gamingText = $activeListItem.data("gaming-text");
			$gamingTime = $activeListItem.data("gaming-time");
			//SET CHARTS ANIMATION DURATION AND TEXT
			//STREAMING
			$(".circle-chart__circle.streaming").css("animation-duration", $streamingTime + "s");
			$(".streaming-chart .change-variable-streaming").html($streamingText);

			//MUSIC
			$(".circle-chart__circle.music").css("animation-duration", $musicTime + "s");
			$(".music-chart .change-variable-music").html($musicText);
			//PHOTOS
			$(".circle-chart__circle.photo").css("animation-duration", $photoTime + "s");
			$(".photo-chart .change-variable-photo").html($photoText);
			//GAMING
			$(".circle-chart__circle.gaming").css("animation-duration", $gamingTime + "s");
			$(".gaming-chart .change-variable-gaming").html($gamingText);
			setTimeout(function() {
				$(".circle-chart__circle").removeClass("reverse");
				$(".circle-chart__circle").css("animation-name", "circle-chart-fill");
			}, 600);
		});

		$('.fact-slideshow').slick({
			slidesToShow: 1,
			dots: false,
			infinite: true,
			prevArrow: $('.fact-prev'),
			nextArrow: $('.fact-next'),
			fade: true,
			speed: 1000
		});

	});
</script>

<?php get_footer(); ?>