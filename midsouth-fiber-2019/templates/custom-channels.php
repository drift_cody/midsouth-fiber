<?php
/*
Template Name: Channel Guide
*/
?>

<?php get_header(); ?>

<main class="channels">
	<section class="s1-channels">
		<h1><?php the_field('s1_main_title'); ?></h1>
		<?php the_field('s1_text'); ?>
	</section>
	<section class="s2-channels">
		<div class="packages">
			<div class="local">
				<a href="?fwp_package=local">
					<?php the_field('s2_local_plus_package'); ?>
				</a>
			</div>
			<div class="expanded">
				<a href="?fwp_package=expanded%2Clocal">
					<?php the_field('s2_expanded_package'); ?>
				</a>
			</div>
			<div class="addon">
				<a href="?fwp_package=add-on">
					<?php the_field('s2_add_on_package'); ?>
				</a>
			</div>
		</div>
		<div class="facet"><?php echo do_shortcode('[facetwp facet="package"]'); ?></div>
		<div class="channels">
			<div class="channel-header">
				<div class="name"><p>Channel Name</p></div>
				<div class="number"><p>Channel No.</p></div>
				<div class="type"><p>Type</p></div>
				<div class="package"><p>Package</p></div>
			</div>
			<?php echo do_shortcode('[facetwp template="channels"]'); ?>
		</div>
		<div class="button-container">
            <a  class="outline-button -orange fwp-load-more">Load More</a>
        </div>
	</section>
	<section class="s3-channels">
		<div class="left">
			<h3><?php the_field('s3_main_text'); ?></h3>
		</div>
		<div class="right">
			<?php the_field('s3_paragraph_text'); ?>
			<a href="<?php the_field('s3_link'); ?>" class="outline-button -blue">Get MidSouth TV</a>
		</div>
	</section>
</main>

<?php get_footer(); ?>