<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<main class="contact">
	<section class="s1-contact">
		<div class="left">
			<div class="text">
				<div class="heading-with-circle">
					<h1 class=" r75 heading-with-circle__heading"><?php the_field('s1_big_text') ?></h1>
					<?php if(get_field('s1_small_text')): ?>
					<p class="-uline heading-with-circle__heading-subtext"><?php the_field('s1_small_text') ?></p>
					<?php endif; ?>
					<div class="heading-with-circle__circle-container">
						<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

							<!-- top circle -->
							<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
						</svg>
					</div>
				</div>
				<div class="contact-info">
					<p class="icon-headphones">
						<a href="tel:<?php the_field('phone_number', options); ?>"><?php the_field('phone_number', options); ?></a>
					</p>
					<p class="icon-clock hours">
						<?php the_field('hours', options); ?>
					</p>
					<p class="address icon-location">
						<?php the_field('address', options); ?>
					</p>
				</div>
			</div>
		</div>
		<div class="right"><?php echo do_shortcode('[contact-form-7 id="118" title="Main Contact Form"]'); ?></div>
		<div class="contact-info mobile">
			<p class="icon-headphones">
				<a href="tel:<?php the_field('phone_number', options); ?>"><?php the_field('phone_number', options); ?></a>
			</p>
			<p class="icon-clock hours">
				<?php the_field('hours', options); ?>
			</p>
			<p class="address icon-location">
				<?php the_field('address', options); ?>
			</p>
		</div>
	</section>
</main>

<script>
	$(document).ready(function(){
		$('.heading-with-circle').on('inview', function(event, isInView) {
			if (isInView) {
				$('.heading-with-circle__circle').addClass('animate');
			} else {
				
			}
		});
	});
</script>

<?php get_footer(); ?>