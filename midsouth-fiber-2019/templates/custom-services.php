<?php
/*
Template Name: Services
*/
?>

<?php get_header(); ?>

<main class="services">
	<section class="s1-services gpad">
		<div class="top">
			<div class="left">
				<?php the_field('s1_title'); ?>
			</div>
			<div class="right">
				<p class="r28"><?php the_field('s1_subtext'); ?></p>
			</div>
		</div>
		<div class="bottom">
			<div class="img-cover">
				<img src="<?php echo esc_url(get_field('s1_image')); ?>" alt="" class="cover">
			</div>
		</div>
	</section>
	<section class="s2-services gpad">
		<div class="top">
			<div class="heading-with-circle">
				<h2 class="r75 heading-with-circle__heading"><?php the_field('s2_title') ?></h2>
				<div class="heading-with-circle__circle-container">
					<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

						<!-- top circle -->
						<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
					</svg>
				</div>
			</div>
			
		</div>
		<div class="bottom">
			<div class="left">
				<div class="img-cover">
					<img src="<?php echo esc_url(get_field('s2_image')); ?>" alt="" class="cover">
				</div>
			</div>
			<div class="right">
				<?php
					if( have_rows('s2_features') ):
						while ( have_rows('s2_features') ) : the_row(); ?>
						<div class="feature ">
							<h3><?php the_sub_field('title'); ?></h3>
							<p class="r20"><?php the_sub_field('description'); ?></p>
						</div>
				<?php   endwhile;
					endif;
				?>
			</div>
		</div>
	</section>
	<section class="s3-services gpad">
		<div class="top">
		<div class="heading-with-circle">
				<h2 class="r75 heading-with-circle__heading"><?php the_field('s3_title') ?></h2>
				<p class="r28"><?php the_field('s3_sub_text') ?></p>
				<div class="heading-with-circle__circle-container">
					<svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

						<!-- top circle -->
						<circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
					</svg>
				</div>
			</div>
			
		</div>
		<div class="bottom">
			<?php
				if( have_rows('s3_plans') ):
					while ( have_rows('s3_plans') ) : the_row(); ?>
					<div class="feature">
						<div class="top-section">
							<h3><?php the_sub_field('title'); ?></h3>
							<p class="r20">Prices starting at</p>
							<div class="price">
								<?php the_sub_field('price'); ?>
							</div>
							<!-- <p class="r20">With one year agreement</p> -->
							<div class="gray_line"></div>
						</div>
						<div class="bottom-section">
							<div class="description">
								<?php the_sub_field('description'); ?>
							</div>
							<a href="<?php echo esc_url(get_sub_field('button_link')['url']); ?>" class="outline-button -orange" target="_blank"><?php echo esc_html(get_sub_field('button_link')['title']); ?></a>
						</div>
					</div>
			<?php   endwhile;
				endif;
			?>
		</div>
	</section>
	<section class="s4-services">
	<div class="photo-row-1x2">
			<div class="photo-row-1x2__photo">
				<div class="photo-row-1x2__image-container">
					<img src="<?php echo esc_url(get_field('s4_image1')); ?>" alt="" class="cover">
				</div>
			</div>
			<div class="photo-row-1x2__photo">
				<div class="photo-row-1x2__image-container">
					<img src="<?php echo esc_url(get_field('s4_image2')); ?>" alt="" class="cover">
				</div>
			</div>
		</div>
		
	</section>
	<section class="s5-services">
		<div class="c-have-question">
				<div class="c-have-question__logo border-logo">
					<img src="<?php the_field('circular_logo', 'option'); ?>" alt="" class="">
				</div>
				<h2 class="c-have-question__header b35 -uline"><?php the_field('s5_title') ?></h2>

			<div class="c-have-question__subtitle"><?php the_field('s5_subtext') ?></div>
			<div class="c-have-question__btn-container">
				<a href="<?php echo esc_url(get_field('s5_message_button_link')['url']); ?>" class="outline-button -orange"><?php echo esc_html(get_field('s5_message_button_link')['title']); ?></a>
			</div>
		</div>
		<div class="c-faq-section">
			<h2 class="c-faq-section__header b35 -uline"><?php the_field('s5_faq_title') ?></h2>
			<div class="c-faq-section__container ">
				<?php $checkbox = '';
				
					if(get_the_title()==='Residential Services'){
						$checkbox='Residential';
					}else{
						$checkbox='Business';
					}
				?>
				<?php if( have_rows('faqs', 151) ): ?>
					<ul class="c-faq-section__list">
					<?php while ( have_rows('faqs', 151) ) :the_row();
					$checkboxItems= get_sub_field('checkbox');
					
					if( $checkboxItems && in_array($checkbox, $checkboxItems)):?>	
						<li class="c-faq-section__question faq-question" aria-expanded="false">
							<p class="c-faq-section__question-title icon-caret"><?php the_sub_field('question');?></p>
							<div class="c-faq-section__question-answer ">
								<?php the_sub_field('answer')?>
							</div>
						</li>
					<?php endif; endwhile;?>
					</ul>
				<?php endif;?>
			</div>
			<div class="c-faq-section__btn-container "> 
				<a href="<?php echo esc_url(get_field('s5_faq_button_link')['url']); ?>" class="outline-button -orange"><?php echo esc_html(get_field('s5_faq_button_link')['title']); ?></a>
			</div>
		</div>
	</section>
</main>

<script>
	function isInView(elem){
		var docTop=$(window).scrollTop();
		var docBottom = docTop + $(window).height();
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();
		return((elemBottom <= docBottom) && elemTop >= docTop);
	}
	$(window).scroll(function(){
		$('.heading-with-circle__circle').each(function(){
			if(isInView(this) === true){
				$(this).addClass('animate');
			}
		});
	});
	$(document).ready(function() {
		$(".s1-services h1").addClass("b130");
		$(".s2-services h3").addClass("b35");
		$(".s3-services div.top p").addClass("r28");
		$(".s3-services h3").addClass("b35u");
		$(".s3-services div.price p").addClass("b75");


		$(".c-faq-section__question").click(function(){
            if($(this).hasClass('active')){    
                $(".c-faq-section__question").removeClass("active");
                $(".title").removeClass("active");
                // $(this).find(".title").removeClass('active');
                $(this).find(".c-faq-section__question-answer").slideUp('slow');
                $(this).attr("aria-expanded", "false");
            }else{
                $(".c-faq-section__question").removeClass("active");
                $(".title").removeClass("active");
                $(".c-faq-section__question-answer").slideUp('slow');
                $(this).addClass("active");
                // $(this).find(".title").addClass('active');
                $(this).find(".c-faq-section__question-answer").slideToggle('slow');
                $(this).attr("aria-expanded", "true");
            }

        });	
	});
</script>

<?php get_footer(); ?>