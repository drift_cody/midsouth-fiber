<?php
/*
Template Name: Streaming
*/
?>

<?php get_header(); ?>

<main class="streaming">
	<section class="s1-streaming gpad">
		<div class="left">
			<h1 class="b130"><?php the_field('s1_text'); ?></h1>
		</div>
		<div class="right">
			<?php if (!empty(get_field('s1_image'))) {
				$image = get_field('s1_image'); ?>
				<img <?php acf_srcset($image['id'], 'large', '100vw'); ?> alt="<?php echo $image['alt']; ?>" />
			<?php } ?>
		</div>
	</section>
	<section class="s2-streaming">
		<div class="left">
			<h2><?php the_field('s2_text'); ?></h2>
		</div>
		<div class="right">
			<a href="/channel-guide" class="outline-button -blue">Channel Guide</a>
		</div>
	</section>
	<section class="s3-streaming">
		<h3 class="b35 -uline">Our Packages</h3>
		<div class="packages">
			<div class="left">
				<?php $left = get_field('s3_left_package'); if( $left ): ?>
					<img src="<?php echo esc_attr( $left['image'] ); ?>" />
					<div class="flexo">
						<div class="text">
							<?php echo $left['text']; ?>
						</div>
						<a href="<?php echo $left['link']; ?>" class="outline-button -orange not-mobile">View Channels</a>
					</div>
					<div class="left-channels">
						<div class="additional-info">
							<?php echo $left['additional_text']; ?>
						</div>
						<div class="channels">
							<p><strong>Included Channels</strong></p>
							<?php echo $left['channels']; ?>
						</div>
					</div>
					<a href="<?php echo $left['text']; ?>" class="outline-button -orange mobile">View Channels</a>
				<?php endif; ?>
			</div>
			<div class="right">
				<?php $right = get_field('s3_right_package'); if( $right ): ?>
					<img src="<?php echo esc_attr( $right['image'] ); ?>" />
					<div class="flexo">
						<div class="text">
							<?php echo $right['text']; ?>
						</div>
						<a href="<?php echo $right['link']; ?>" class="outline-button -orange not-mobile">View Channels</a>
					</div>
					<div class="right-channels">
						<p><strong>Included Channels</strong></p>
						<?php echo $right['channels']; ?>
					</div>
					<a href="<?php echo $right['text']; ?>" class="outline-button -orange mobile">View Channels</a>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="s4-streaming">
		<h3 class="b35 -uline">Add Ons</h3>
		<div class="addons">
			<div class="left">
				<?php the_field('s4_left_add_on'); ?>
			</div>
			<div class="middle">
				<?php the_field('s4_middle_add_on'); ?>
			</div>
			<div class="right">
				<?php the_field('s4_right_add_on'); ?>
			</div>
		</div>
	</section>
	<section class="s5-streaming">
		<div class="box">
			<div class="left">
				<h3><?php the_field('s5_main_text'); ?></h3>
				<div class="mobile"><?php the_field('s5_paragraph_text'); ?></div>
			</div>
			<div class="right">
				<div class="not-mobile"><?php the_field('s5_paragraph_text'); ?></div>
				<a href="<?php the_field('s5_link'); ?>" class="solid-button">Get MidSouth TV</a>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>