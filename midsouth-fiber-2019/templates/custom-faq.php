<?php
/*
Template Name: FAQ
*/
?>
<main class="faq">
<?php get_header(); ?>
<section class="s1-faq gpad12">
    <div class="heading-with-circle">
        <h1 class=" r75 heading-with-circle__heading"><?php the_field('title') ?></h1>
        <?php if(get_field('subtext')): ?>
        <p class="-uline heading-with-circle__heading-subtext"><?php the_field('subtext') ?></p>
        <?php endif; ?>
        <div class="heading-with-circle__circle-container">
            <svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

                <!-- top circle -->
                <circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
            </svg>
        </div>
    </div>
</section>
<section class="s2-faq gpad12">
    <?php if( have_rows('faqs') ): ?>
        <ul>
		<?php while ( have_rows('faqs') ) :the_row(); ?>    
            <li class="faq-question" aria-expanded="false">
                <p class="title r35 icon-caret"><?php the_sub_field('question');?></p>
                <div class="answer">
                    <?php the_sub_field('answer')?>
                </div>
            </li>
        <?php endwhile;?>
        </ul>
    <?php endif;?>
</section>
<section class="s3-faq gpad">
    <div class="logo-text-btn-block">
        <div class="logo-text-btn-block__logo">
            <img src="<?php the_field('circular_logo', 'option'); ?>" alt="logo">
        </div>
        <h2 class="logo-text-btn-block__heading b35 -uline">Don't see your question?</h2>
        <p class="logo-text-btn-block__text r20">We are happy to help you with any questions you have regarding your service needs. Contact us through our form or give us a call at <a href="tel:936-825-5100">936-825-5100</a>.</p>
        <div class="logo-text-btn-block__btn-container">
            <a href="/contact" class="outline-button -orange">Send us a message</a>
        </div>
    </div>
</section>
</main>

<script>
    function isInView(elem){
		var docTop=$(window).scrollTop();
		var docBottom = docTop + $(window).height();
		var elemTop = $(elem).offset().top;
		var elemBottom = elemTop + $(elem).height();
		return((elemBottom <= docBottom) && elemTop >= docTop);
	}
	$(window).scroll(function(){
		$('.heading-with-circle__circle').each(function(){
			if(isInView(this) === true){
				$(this).addClass('animate');
			}
		});
	});
    $(document).ready(function() {
        $('.heading-with-circle__circle').each(function(){
			if(isInView(this) === true){
				$(this).addClass('animate');
			}
		});
		$(".s1-faq h1").addClass("r75");


        $(".faq-question").click(function(){
            if($(this).hasClass('active')){    
                $(".faq-question").removeClass("active");
                $(".title").removeClass("active");
                $(this).find(".title").removeClass('active');
                $(this).find(".answer").slideUp('slow');
                $(this).attr("aria-expanded", "false");
            }else{
                $(".faq-question").removeClass("active");
                $(".title").removeClass("active");
                $(".answer").slideUp('slow');
                $(this).addClass("active");
                $(this).find(".title").addClass('active');
                $(this).find(".answer").slideToggle('slow');
                $(this).attr("aria-expanded", "true");
            }

        });	
	});
</script>

<?php get_footer(); ?>