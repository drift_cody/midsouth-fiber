<?php
/*
Template Name: Blog Post
*/
?>

<?php get_header(); ?>
<main class="blog gpad12">
    <section class="s1-blog">
        <div class="heading-with-circle">
            <h1 class=" r75 heading-with-circle__heading"><?php the_field('title') ?></h1>
            <p class="-uline heading-with-circle__heading-subtext">News & updates</p>
            <div class="heading-with-circle__circle-container">
                <svg class="heading-with-circle__svg" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">

                    <!-- top circle -->
                    <circle class="heading-with-circle__circle" stroke="#FF6B00" stroke-width="1" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                </svg>
            </div>
        </div>
    </section>
    <section class="s2-blog">
        <div class="posts">
            <ul>
                <?php echo facetwp_display( 'template', 'blog_posts' ); ?>
            </ul>
        </div>
        <div class="button-container">
            <a  class="outline-button -orange fwp-load-more">Load More</a>
        </div>
    </section>
    <section class="s3-blog">
        <div class="logo-text-btn-block">
            <div class="logo-text-btn-block__logo">
                <img src="<?php the_field('circular_logo', 'option'); ?>" alt="logo">
            </div>
            <h2 class="logo-text-btn-block__heading b35 -uline">Get started with fiber internet</h2>
            <!-- <p class="logo-text-btn-block__text r20">.</p> -->
            <div class="logo-text-btn-block__btn-container">
            <a href="https://mss.crowdfiber.com/front_end/zones" target="_blank" class="outline-button -orange">Search for your area</a>
            </div>
        </div>
        
    </section>
</main>
<script>
	$(document).ready(function(){
		$('.heading-with-circle').on('inview', function(event, isInView) {
			if (isInView) {
				$('.heading-with-circle__circle').addClass('animate');
			} else {
				
			}
		});
	});
</script>

<?php get_footer(); ?>