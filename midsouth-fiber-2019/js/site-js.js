var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= 0 &&
        bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};
window.addEventListener("scroll", function(){
    $('.heading-with-circle__circle').each(function() {
        if (isInViewport(this)) {
            console.log("is in view");
            $(this).addClass('animate');
        }
    });
});