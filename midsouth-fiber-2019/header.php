<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php
		/*
				 * Print the <title> tag based on what is being viewed.
				 */
		global $page, $paged;

		wp_title('|', true, 'right');

		// Add the blog name.
		bloginfo('name');
		?>
	</title>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	    	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	<!--[if IE]>
			<style>
				
			</style>
		<![endif]-->
	<?php wp_head(); ?>
</head>

<body>

	<header class="header">
		<div class="header__promo header-promo gpad">
			<!-- <a href="#" class="icon-caret"> MidSouth Electric Co-Op</a> -->
			<!-- <p class="header__promo-text">Bundle your internet with tv today and get a discount!</p> -->
		</div>
		<div class="header__container header-navigation gpad">
			<a href="/" class="header__logo logo"><img src="<?php echo esc_url(the_field('header_logo', 'option')) ?>" alt=""></a>
			<button class="header__hamburger hamburger" aria-expanded="false" aria-label="Mobile Menu Click to Open">
				<div class="header__hamburger-bar bar"></div>
			</button>
			<div class="header__links header-links">
				<nav class="header__navigation">
					<?php wp_nav_menu(array('theme_location' => 'primary', 'container' => 'div', 'container_id' => 'nav-menu')); ?>
				</nav>
				<a href="https://mss.crowdfiber.com/" target="_blank" class="outline-button -orange">Get Fiber Internet</a>
				<a href="https://mybroadbandaccount.com/MidSouthFiber/" target="_blank" class="outline-button -orange">Log into account</a>
			</div>
		</div>
	</header>